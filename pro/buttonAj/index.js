import React from 'react';
import { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import ActionButton from 'react-native-action-button';
import {Icon} from 'react-native-elements';
import {APP_COLORS} from '../styles/color'

const ButtonAdd = () => (
    <ActionButton 
    buttonColor = {APP_COLORS.purple} 
    icon = {<Icon color ={APP_COLORS.blue} name={'add'} />}
    onPress= {() => onPress()}
    />
);

function onPress(){
    console.log('onpress');
}

export default ButtonAdd;