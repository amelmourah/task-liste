import{StyleSheet} from 'react-native'
import {APP_COLORS} from '../styles/color'


export const style = StyleSheet.create({
    subHeader : {
        backgroundColor: APP_COLORS.blue,
        height: 50
    },
    Header : {
        backgroundColor: APP_COLORS.rosec,
        height: 150,
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        color: APP_COLORS.blue,
        fontSize : 30
    },
    bottom: {
        flex: 1,
        justifyContent: 'flex-end',
        marginBottom: 36
      },
      container: {
        flex: 1,
        marginTop: 160,
      },
      item: {
        backgroundColor: '#f9c2ff',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
      },
      title: {
        fontSize: 32,
      },
})